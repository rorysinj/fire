var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin'); 

var extractPlugin = new ExtractTextPlugin({
    filename: 'bundle.css'
 });
 
var config = {
    entry: [path.resolve(__dirname, 'src/Application.js')],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
			{
				test: /\.css$/,
				use: [
					'style-loader',  // 這個後 (順序很重要)
					'css-loader' // 這個先
				]
			},
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: extractPlugin.extract({
                    use: [
						'css-loader', 
						'sass-loader'
					]
                })
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin(function(){
        }),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
        extractPlugin
    ]
};

module.exports = config;