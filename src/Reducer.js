const UPDATE_DATA = 'UPDATE_DATA';

const initialState = {
    "list": {},
    "search": false,
    "pageIndex":0,
    "selectedCatetory":"automation",
    "selectedECatalog":"ALL"
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case UPDATE_DATA:
            return {
                ...state,
                ...action.data
            }
        default:
            return state;
    }
}

export function changeList(value) {
    return {
        type: UPDATE_DATA,
        data: {
            "list":value
        }
    };
}

export function changeSearch(value) {
    return {
        type: UPDATE_DATA,
        data: {
            "search":value
        }
    };
}

export function changePageIndex(value) {
    return {
        type: UPDATE_DATA,
        data: {
            "pageIndex":value,
            "search":false
        }
    };
}

export function changeSeletedCatetory(value) {
    return {
        type: UPDATE_DATA,
        data: {
            "selectedCatetory":value
        }
    };
}

export function changeSelectedECatalog(value) {
    return {
        type: UPDATE_DATA,
        data: {
            "selectedECatalog":value
        }
    };
}