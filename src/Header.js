import React from 'react';
import ReactDOM from 'react-dom';
import {changePageIndex,changeSearch} from './Reducer';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Store from './Store';
const clickStyle = {fontSize:13, lineHeight:4.5, height:60, cursor:'pointer', textAlign:'center'}

class Header extends React.Component {
    constructor(props){
        super(props)
        this.onChange = this.onChange.bind(this)
    }
    
    onChange(e) {
        this.props.changeSearch(e.target.value);
    }

    render() {
        return (
            <div style={{width:'100%', height:180}}>
                <div className="container" style={{height:120}}>
                    <div className="row">
                        <div className="col-md-1"/>
                        <div className="col-md-2">
                            <div>
                                防災知識網
                                <img src="assets/testImage.jpg"  onClick={()=>{window.location.pathname="/";}} style={{cursor:'pointer', position:'absolute', left:20, top:10, height:100, width: 200, background: 'black'}} />
                            </div>
                        </div>
                        <div className="col-md-6"/>
                        <div className="col-md-2">
                            全文檢索
                            <input type="text" placeholder=""/>
                            <button>搜尋</button>
                        </div>
                    </div>
                </div>
          </div>
        );
    }
}

export default connect(state => ({

}), dispatch => bindActionCreators({changePageIndex,changeSearch}, dispatch))(Header);