import React from 'react';
import ReactDOM from 'react-dom';
import {changePageIndex, changeSeletedCatetory} from './Reducer';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


export default class Footer extends React.Component {
    render() {
        return (
            <div style={{background:'#BDBDBD', height:120}}>
                <div className="row">
                    <div className="col-md-1"/>
                    <div className="col-md-7">
                        Footer
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(state => ({
}), dispatch => bindActionCreators({changePageIndex, changeSeletedCatetory}, dispatch))(Footer);