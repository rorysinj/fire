import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Store from './Store';
import { Router, Route, IndexRoute, Link, hashHistory } from 'react-router'

import Header from './Header';
import Home from './Home';
import Footer from './Footer';

import './scss/theme.scss';
import './scss/style.scss';

class Application extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageIndex: 0
        }
    }
    
    render() {

        if (this.props.search==false) {
            if (document.getElementById("search")){
                document.getElementById("search").value = "";
            }
        }
        return (
            <div id="app">
                <Header/>
                    <Home/>
                <Footer/>
            </div>
        );
    }
}

var App = connect(state => ({
    search:state.search,
    pageIndex:state.pageIndex
}), dispatch => bindActionCreators({}, dispatch))(Application);

ReactDOM.render(
    <Provider store={Store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);


  