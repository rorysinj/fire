import React from 'react';
import ReactDOM from 'react-dom';
import {changePageIndex, changeSeletedCatetory} from './Reducer';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Slider from './Slider';

var titleStyle={color:'#DA251C',fontSize:'24pt', fontWeight:'bold', textShadow: 'grey 0.1em 0.1em 0.2em' }

class Home extends React.Component {

    render() {
        return (
            <div style={{minHeight:'calc(100vh - 300px)'}}>
                <div style={{background:'white', height:'calc(100vh - 208px)', maxHeight:'500px', padding:'0 100px'}}>
                    <Slider/>
                </div>
            </div>
        );
    }
}

export default connect(state => ({
    
}), dispatch => bindActionCreators({changePageIndex, changeSeletedCatetory}, dispatch))(Home);