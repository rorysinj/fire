import React from 'react';
import ReactDOM from 'react-dom';
import ReactSlider from 'react-slick';
  
var imgStyle = {background:'white', height:'calc(100vh - 208px)', maxHeight:'480px'}

export default class Slider extends React.Component {
  constructor(props) {
    super(props)
    this.next = this.next.bind(this)
    this.previous = this.previous.bind(this)
  }

  componentDidMount(){

      var timer = setInterval(()=>{
        try{
          this.slider.slickNext()
        }
        catch(e){
          clearInterval(timer)
        }
      },3000);
  }

  next() {
    this.slider.slickNext()
  }
  previous() {
    this.slider.slickPrev()
  }
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
      <div>
        <ReactSlider ref={c => this.slider = c } {...settings}>
          <div className="Mobile" style={imgStyle}><img style={{height:'100%', margin:'auto'}} src="assets/1.jpg"/></div>
          <div className="Mobile" style={imgStyle}><img style={{height:'100%', margin:'auto'}} src="assets/2.jpg"/></div>
          <div className="Mobile" style={imgStyle}><img style={{height:'100%', margin:'auto'}} src="assets/3.jpg"/></div>
          <div className="Mobile" style={imgStyle}><img style={{height:'100%', margin:'auto'}} src="assets/4.jpg"/></div>
        </ReactSlider>
      </div>  
    );
  }
}