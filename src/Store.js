
import {combineReducers, createStore} from 'redux';
import reducer from './Reducer'

export default createStore(
    reducer
);
